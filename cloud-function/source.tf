resource "random_uuid" "this" {
  keepers = {
    for filename in fileset(path.module, "**/*"):
    filename => filemd5("${path.module}/${filename}")
  }
}

data "archive_file" "source_archive" {
  output_path = "${path.module}/temp/source-${random_uuid.this.result}.zip"
  source_dir  = var.source_path
  type        = "zip"
}

resource "google_storage_bucket_object" "source_object" {
  name   = "source-${local.function_name}-${data.archive_file.source_archive.output_md5}.zip"
  bucket = var.cf_src_bucket
  source = data.archive_file.source_archive.output_path
  depends_on = [data.archive_file.source_archive]
}

