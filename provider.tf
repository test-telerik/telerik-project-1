provider "google" {
  credentials = file("./creds/tf-account.json")
  project     = "windy-bearing-338808"
  region      = "europe-west1"
  version     = "~> 3.45.0"
}