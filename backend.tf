terraform {
  backend "gcs" {
    bucket      = "tf-state-telerik" 
    credentials = "./creds/tf-account.json"          
  }
}
