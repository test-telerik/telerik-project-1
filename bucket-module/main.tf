resource "google_storage_bucket" "bucket" {

  name       = var.bucket_module_name
  project    = var.project_id
  location   = var.region
}
