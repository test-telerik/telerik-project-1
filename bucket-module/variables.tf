variable "project_id" {
  description = "Id of the project"
  type        = string
}

variable "bucket_module_name" {
  description = "Name of the bucket"
  type        = string
}

variable "region" {
  description = "Google Cloud region"
  type        = string
}