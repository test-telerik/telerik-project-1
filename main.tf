module "bucket" {
  source  = "./bucket-module"

  bucket_module_name  =  "bucket-telerik"
  project_id          =  "windy-bearing-338808"
  region              =  "europe-west1"
}

module "telerik_http_function" {

  source           = "./cloud-function"
  sls_project_name =  "windy-bearing-338808"
  sls_project_env  =  "main"
  project          =  "windy-bearing-338808"
  cf_src_bucket    =  "bucket-telerik"
  #alert_channel    =  var.alert_channel
  entry_point      =  "hello_world"
  source_path      =  "./source"

  trigger_type = "http"
  function_name       = "telerik-hello-world-cloud-function"

  region              = "europe-west1"
  runtime             = "python37"
  available_memory_mb = 256
  timeout             = 60

  service_account_email = "tf-63-463@windy-bearing-338808.iam.gserviceaccount.com"
  environment_variables = {}

  invokers = [
    "allUsers",
  ]
}

//  module "test_application_topic_alerting" {

//   source           = "./cloud-function"
//   sls_project_name =  "windy-bearing-338808"
//   sls_project_env  =  "main"
//   project          =  "windy-bearing-338808"
//   cf_src_bucket    =  "bucket-telerik"
//   #alert_channel    =  var.alert_channel
//   entry_point      =  "hello_world"
//   source_path      =  "./source"

//    trigger_type     = "topic"
//    function_name    = "gitlab-auditlogs-to-slack-cloud-function"

//    region              = "europe-west1"
//    runtime             = "python38"
//    available_memory_mb = 512
//    timeout             = 540

//    service_account_email = var.service_account_email
//    environment_variables = {
//        SLACK_TOKEN = "xoxb-200324599094-1666427293479-L54LD1NWTXngvXPAxEAPOUYk",
//        GITLAB_TOKEN = "Zt8ocXt7CJ9T6wFVwxTX"
//    }

//    trigger_event_type     = "google.pubsub.topic.publish"
//    #trigger_event_resource = "topic-id"
//  }
